var prompt = require('prompt');
var colors = require("colors/safe");
prompt.message = colors.rainbow("Question!");
prompt.start();

prompt.get(['param1', 'param2', 'param3', 'param4'], function (err, result) {
  if (err) {
    return onErr(err);
  }
  console.log('Command-line input received:');
  console.log(colors.cyan('  輸入1: ' + result.param1));
  console.log(colors.cyan('  輸入2: ' + result.param2));
  console.log(colors.cyan('  輸入3: ' + result.param3));
  console.log(colors.cyan('  輸入4: ' + result.param4));
  result.param1 = parseInt(result.param1);
  result.param2 = parseInt(result.param2);
  result.param3 = parseInt(result.param3);
  result.param4 = parseInt(result.param4);
  //==================================
  //把邏輯寫在這個下面
  // 輸入1使用 result.param1
  // 輸入2使用 result.param2
  // //
  // console.log(0.4 + 0.2 == 0.6);  

  // (0.4+0.2).toFixed(10);
  // console.log(parseFloat (0.4+0.2).toFixed(10)===0.6);
  // console.log(parseFloat((0.4+0.2).toFixed(10)) === 0.6) 
  //-------------------------------
  //   var a = result.param1;
  //   var b = result.param2;
  //   function sum(a,b) {
  //     return a+b;
  //   }
  //   console.log(sum(2,3));
  //   //-----------------------
    var a = result.param1;
    var b = result.param2;
  function sum(a) {
    return function(b){
      return a+b;
    };
  }
   sum(2)(3);

  console.log(sum(2)(3));
  // var  add2 = sum(2);
  // console.log(add2(3));
  //---------------------------------
  // function makeFunc() {
  //   var name = "Mozilla";

  //   function displayName() {
  //     console.log(name);
  //   }
  //   return displayName;
  // }

  // var myFunc = makeFunc();
  // makeFunc()();
//----------------------------------
  // var a = result.param1;
  // var b = result.param2;
  // if(a>b){
  // var t = 0;
  // var y = b;
  // for(var i = 0; y<a; i++){
  //   y = b+i;
  //   console.log('y'+y);
  //   t = t+y;
  // }

  //   console.log('sum'+t);
  // }else if(a<b){

  //   var t = 0;
  //   var x = a;
  //   for(var i = 0; x<b; i++){

  //      x = a+i;
  //     console.log("x "+x);
  //    t = t + x;
  //    // console.log("t :"+t);
  //   }

  //   console.log("last "+t);

  // }else{
  //   console.log('0');
  // }
  //-------------------------------------------------
  // function dogHouse() {

  // var count = 0;
  // function countDog() {
  //   count = count + 1;
  //   console.log(count+'dog');
  // }
  // return countDog;

  // }

  // const countDog = dogHouse();

  // function catHouse() {

  // var count = 0;
  // function countCat() {
  //   count = count+1;
  //   console.log(count+'cat');
  // }
  // return countCat;

  // }
  // const countCat = catHouse();

  // countDog();
  // countCat();
  // countDog();
  // countCat();
  // countDog();

  function makeFunc() {
    var name = "Mozilla";

    function displayName() {
      console.log(name);
    }
    return displayName;
  }

  var myFunc = makeFunc();
  makeFunc()();

  // test = (function () {
  //   console.log('XXXXXXXXXXXXXXX');
  //   return function () {
  //     console.log("YYYYYYYYYYY");
  //   }
  // }())

  // test();
  // test();


  // q1(result.param1);
  // q3(result.param1);
  // q4(result.param1);
  // q9(result.param1,result.param2);
  // q10(result.param1,result.param2);
  //q11(result.param1, result.param2);
  // q12(result.param1);
  // q16(result.param1);
  // q24(result.param1, result.param2);


  //  q21(result.param1);
  //==================================
});

function onErr(err) {
  console.log(err);
  return 1;
}
//=============================================
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//=============================================

function q1(param1) {
  console.log(param1 / 60 + ':' + param1 % 60);
}


function q2(val) {
  console.log(Math.floor(val / 11) * 1000 + (val % 11) * 100);
}


function q3(param1) {
  var a = (1 * param1 + 3);
  var b = (param1);
  var c = (2 * param1 - 5);
  var d = (Math.floor(c / 10) * 10 + a % 10);


  console.log('大綠=' + a + '小綠=' + b + '阿藍=' + c + '阿紫=' + d)
}


function q4(val) {

  var pass = 'pass';
  var FAIL = 'FAIL';
  if (val >= 60 && val <= 100) {
    console.log(pass);
  } else {
    console.log(FAIL);
  }

}

function q5(val) {

  if (val >= 75 && val <= 90) {
    console.log('yes');
  } else {
    console.log('no');
  }

}


function q6(val, val2) {

  var a = val;
  var b = val2;

  if (a > b) {
    console.log('a>b');
  } else if (a < b) {
    console.log('a<b');
  } else {
    console.log('a=b');
  }

}


function q7(val) {

  if ((val % 4 === 0 && val % 100 != 0) || val % 400 === 0) {
    console.log('閏年');
  } else {
    console.log('平年');
  }

}


function q8(val) {

  if (val <= 100 && val >= 90) {
    console.log('A');
  } else if (val < 90 && val >= 80) {
    console.log('B');
  } else if (val < 80 && val >= 70) {
    console.log('C');
  } else if (val < 70 && val >= 60) {
    console.log('D');
  } else {
    console.log('E');
  }

}


function q9(val1, val2) {


  if (val1 == 14 && val2 >= 20 && val2 <= 59) {
    console.log('yes');
  } else if (val1 == 15) {
    console.log('yes');
  } else if (val1 == 16 && val2 >= 0 && val2 <= 40) {
    console.log('yes');
  } else {
    console.log('no');
  }

  if ((val1 == 14 && val2 >= 20 && val2 <= 59) ||
    (val1 == 15) ||
    (val1 == 16 && val2 >= 0 && val2 <= 40)) {
    console.log('yes');
  } else {
    console.log('no');
  }

  // if (result.param1 == 14) {
  //   if (result.param2 >= 20 && result.param2 <= 59) {
  //     console.log('yse1');
  //   }
  // } else if (result.param1 == 15) {
  //   console.log('yse');
  // } else if (result.param1 == 16) {
  //   if (result.param2 > 0 && result.param2 <= 40) {
  //     console.log('yse');
  //   }
  // } else {
  //   console.log('no');
  // };

  var starttime = 14 * 60 + 20;
  var endttime = 16 * 60 + 40;

  var val = val1 * 60 + val2;

  if (val >= starttime && val <= endttime) {
    console.log('M yes');
  } else {
    console.log('M no');
  }


}


function q10(val1, val2) {


  if (val2 % 3 == 2) {
    console.log('200');
  } else if (val1 % 2 == 1) {
    console.log('100');
  } else if (val1 == val2) {
    console.log('50');
  } else {
    console.log('0');
  }

}

/*function q11(val1, val2) {

  var total = 0;
  if (val1 % 2 == 1) {
    total = 100;
    //fisrt = 100;
  }
  if (val2 % 3 == 2) {
    total = total + 200;
    // second = 200;
  }
  if (val1 == val2) {
    total = total + 50;
    // eq = 50;
  }

  console.log(total);
}*/

function q11(val1, val2) {


  var total = 0;
  if (val1 % 2 == 1) {
    total = 100;
  }
  if (val2 % 3 == 2) {
    total = total + 200;
  }
  if (val1 == val2) {
    total = total + 50;
  }
  console.log(total);


  //========================================

  var a = 100;
  var b = 200;
  var c = 50;

  if (val1 % 2 == 1 && val2 % 3 == 2 && val1 == val2) {
    console.log(a + b + c);
  } else if (val1 % 2 == 1 && val2 % 3 == 2) {
    console.log(a + b);
  } else if (val1 % 2 == 1 && val1 == val2) {
    console.log(a + c);
  } else if (val1 % 2 == 1) {
    console.log(a);
  } else if (val2 % 3 == 2) {
    console.log(b);
  } else if (val1 == val2) {
    console.log(c);
  } else {
    console.log('0');
  }


}

function q12(val1) {

  /*var  value = 0;
  while(true)
  {
    if(val1 % 2 == 1)
        break;
    value  =  val1 / 2;
    val1 = value;
  }
  console.log(val1);
  */
  //=============================
  while (true) {

    if (val1 % 2 == 1)
      break;

    A = val1 / 2
    val1 = A;
  }

  for (; val1 % 2 == 0;) {
    // val1 = val1/2
    A = val1 / 2
    val1 = A;
  }

  console.log(val1);


  //=============================
  // var value = val1;
  // while (true) {
  //   if (value % 2 == 1) {
  //     console.log('OK' + value);
  //     break;
  //   }
  //  val1 = value/2;
  //  value=val1;
  // }

}


function q13(val1, val2) {

  var sum = 0;
  var x = val1;
  for (var i = 0; x < val2; i++) {

    x = x * 3;
    sum = sum + 1;
    console.log(sum);

  }

}



function q14(val) {

  //  var num = 0;
  // for(var i = 0 ; i < 3; i++)
  // {
  //   //var onum = num + 1;
  //  // num = onum;
  //   num = num + 1;  // num++;
  //   num = num + 1;

  //   console.log(num);
  // }


  var num = 1;

  while (result.param1 !== 1) {

    if (result.param1 % 2 == 1) {
      a = result.param1 * 3 + 1;
      result.param1 = a;
      num = num + 1;
    }

    if (result.param1 % 2 == 0) {
      b = result.param1 / 2;
      result.param1 = b;
      num = num + 1;
    }

  }
  console.log(num);
  //======================================
  var num = 1;

  while (result.param1 !== 1) {

    if (result.param1 % 2 == 1) {
      a = result.param1 * 3 + 1;
      result.param1 = a;
      num = num + 1;
    } else {
      b = result.param1 / 2;
      result.param1 = b;
      num = num + 1;

    }

  }
  console.log(num);


}

function newFunction() {
  return 1;
}


function q15() {


  //---------------------------------------------
  // console.log(result.param1);
  // console.log(typeof(result.param1));
  // for (var i = 0; i < result.param1.length; i++) {
  //   console.log('result.param1[' + i +']:' + result.param1[i]);
  // }
  //---------------------------------------------
  //  var i = result.param1;
  // var sum = 0;
  // for(i=0; i<=5; i++ ){
  //   sum = sum +i;

  // }
  // console.log(sum);

  //----------------------------------
  var stringtotal = result.param1.length;
  var sum = 0;


  for (var i = 0; i < stringtotal; i++) {

    sum = sum + parseInt(result.param1[i]);
  }
  console.log(sum)
  //----------------------------------------------------

  //    var a = Math.floor(result.param1%10);  //123.(4)
  //    console.log(a);
  //    var aa = Math.floor(result.param1/10);   ///(123).4

  //   // sum = sum+a;
  //   var b= Math.floor(aa%10);     //12.(3)
  //  console.log(b);
  //  var bb = Math.floor(aa/10);       //(12).3
  //   // sum = sum+b;
  //   var c = Math.floor( bb%10);          //1.(2)
  //   console.log(c);
  //   var cc = Math.floor(bb/10);             //(1).2
  //   // sum = sum+c;
  //   var d = Math.floor( cc%10);             //1
  //   console.log(d);
  //------------------------------------------------
  for (; result.param1 % 10 > 0;) {
    var x = result.param1 % 10;
    result.param1 = Math.floor(result.param1 / 10);
    sum = sum + x;
  }
  sum = sum + result.param1;
  console.log(sum)

}

function q16(val) {

  for (var i = 1; i <= val; i++) {

    console.log('I love you');

  }

}


function q17(val) {

  var a = val;

  for (var i = 0; i <= val; i++) {
    console.log(a);
    a = a - 1;
  }


}

function q18(val1, val2) {

  var a = val1;
  var b = val2;
  var sum = 1;
  var x = a;


  for (var i = 0; a - i >= a - b + 1; i++) {

    x = a - i;
    sum = sum * x;

  }
  console.log(sum);

}

function q19(val1, val2) {

  var a = val1;
  var b = val2;
  var t = 0;
  var x = a;
  for (var i = 0; x < b; i++) {

    x = a + i;
    console.log("x " + x);
    t = t + x;
    // console.log("t :"+t);
  }

  console.log("last " + t);
  //------------------------------
  var a = result.param1;
  var b = result.param2;
  var t = 0;
  var x = a;
  for (var i = 0; x < b; i++) {

    x = a + i;
    console.log("a " + a);
    console.log("i " + i);
    console.log("x " + x);
    console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    t = t + x;
    // console.log("t :"+t);
  }

  console.log("last " + t);



}

function q20(val1, val2) {

  var a = result.param1;
  var b = result.param2;
  if (a > b) {
    var t = 0;
    var y = b;
    for (var i = 0; y < a; i++) {
      y = b + i;
      console.log('y' + y);
      t = t + y;
    }

    console.log('sum' + t);
  } else if (a < b) {

    var t = 0;
    var x = a;
    for (var i = 0; x < b; i++) {

      x = a + i;
      console.log("x " + x);
      t = t + x;
      // console.log("t :"+t);
    }

    console.log("last " + t);

  } else {
    console.log('0');
  }



}

function q21(val1, val2) {

  var word = val1.split("+");
  if (word.length >= 2) {
    console.log(parseInt(word[0]) + parseInt(word[1]));
  }
  var word = val1.split("-");
  if (word.length >= 2) {

    console.log(parseInt(word[0]) - parseInt(word[1]));
  }
  var word = val1.split("*");
  if (word.length >= 2) {

    console.log(parseInt(word[0]) * parseInt(word[1]));
  }
  //---------------------------------------------------

  var xx = 0;
  var word = val1.split("+");
  if (word.length >= 2) {
    xx = parseInt(word[0]) + parseInt(word[1]);
  }
  var word = val1.split("-");
  if (word.length >= 2) {

    xx = parseInt(word[0]) - parseInt(word[1]);
  }
  var word = val1.split("*");
  if (word.length >= 2) {

    xx = parseInt(word[0]) * parseInt(word[1]);
  }

  console.log(xx);
  //  console.log(xx);

}


function q22(val1, val2, val3, val4) {

  var x1 = val1;
  var y1 = val2;
  var x2 = val3;
  var y2 = val4;
  var dx = 0;
  var dy = 0;
  var ans = 0;
  if (x1 > x2) {
    dx = x1 - x2;
  } else {
    dx = x2 - x1;
  }

  if (y1 > y2) {
    dy = y1 - y2;
  } else {
    dy = y2 - y1;
  }
  console.log('dx:' + dx);
  console.log('dy:' + dy);
  dx = dx * dx;
  dy = dy * dy;

  ans = Math.sqrt(dx + dy);

  console.log(ans)
  console.log(Math.round(ans) / 1000);


}


function q23(val) {

  var a = val1;
  var b = 0;
  var c = 0;
  var freeice = 0;
  var end = 0;
  var eat = 0;

  for (; a >= 5;) {

    freeice = Math.floor(a / 5);
    end = a % 5;
    eat = eat + a - end;
    a = freeice + end;
  }
  console.log(eat + a);



}


function q24(val1, val2) {


  var a = val1;
  var b = val2;


  var c = Math.min(a, b);
  a = Math.max(a, b);
  b = c;
  if (b === 0) {
    console.log('xxxxxxx');
  }
  var c = a % b;
  while (c > 0) {
    a = b;
    b = c;
    c = a % b;
  }
  console.log(b);


}